# AWS and Bash Exercise around Environment Variables SCRIPT

This exercise will include: 
- AWS EC2
- SSH connection
- Running a simple bash script

You have just done an exercise were you create a machine and set some variables and installed some simple software like cowsay and nodejs. 

Your task is to now translate this into a script so you can set up any AMAZON linux machine to said state. 


### Other requierment: 
- These bash scripts should be in an organised repo with all the excercises or sperate repo's on bitbucket. 

### User stories

**User Story 1**
As a DevOps user I need to make a lot of machines that have main USER STORIES up to the EXTRAS from the previous user stories in `101_README_AWS_ENVariables` and I want a script I can run, so that my setup time is minimal. 

**User Story 2 EXTRA**
As a DevOps user I need to make a lot of machines and some of them include the task in the EXTRA user stories in `101_README_AWS_ENVariables` and I want a script I can run, so that my setup time is minimal. 

