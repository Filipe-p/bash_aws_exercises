# AWS and Bash Exercise around Environment Variables 

This exercise will include: 
- AWS EC2
- SSH connection
- Setting enviroment variables

### Description

You have learned how to set environment variables in you machine with ohh-my-zsh (a type of bash terminal) and placing variables in the path. 

In aws, machines have bash without ohh-my-zsh. They use another file! But i want this to be permanant! 

### Task & User stories 

**User story 1**
As a DevOps User, i want to have a ec2 machine that is secure to one ip and an SSH key, so that the machine is generally secured and is not used in malicious activities.

**User Story 2**
As a DevOps User, i want the machine to be an AMAZON Linux machine with small processor and very minimal disk space, so that we keep cost down.

**User Story 3**
As a DevOps user, I want this machine to the following important Environment Variables always set and available for child process, so that I can use them through out the machine.

```
# List of Variables: 

SECRET_API_TFL_TOKE = 123000494003ff

PUBLIC_API_TFL_TOKE = jon@gmail.com

SECRET_NUMBER = 48

```

**User Story 4 Extra**
As a DevOps user, I want this machine to have the cowsay installed so my terminal is funky.

Hint
Search for package managers and how to install packages on rhel.

**User Story 5 Extra**
As a DevOps user, I want this machine to have python installed and nodeJS so that I can do some programming.


