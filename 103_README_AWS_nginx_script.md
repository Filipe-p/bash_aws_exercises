# AWS and Bash Exercise - Nginx Basic Setup

This exercise will include: 
- AWS EC2
- SSH connection
- Running a simple bash script
- Installing nginx and other software
- BASIC Configuring of nginx 
- Starting and stoping services 

### Descipriton 

You just finished installing nginx. Now we need a script to install it so we don't have to do it again. 

See the following user stories 

### User stories 
**User Story 1**
As a DevOps user I need to be able to install a reverse proxy fast AND edit the welcome page, so that setup time is minimal.

**User Story 2**
As a DevOps user I need the welcome to show my name and some coold detaila about myself, so that I can show my clients. 


**User Story 3**
As a DevOps user I need the server port 80 to be open to the world. So anyone can acess BUT ONLY PORT 80! Don't open port 22 to the world. 



### Acceptance criterea

**1** 
Nginx is install 

**2** 
Nginx is running 

**3** 
Nginx default page shows your name and details

**4**
All the pre-requisits to run the script and instructions are in a MD file with the script 

**5**
Is it's own project on Bitbucker 